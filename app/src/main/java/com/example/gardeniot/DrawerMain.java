package com.example.gardeniot;


import android.app.FragmentManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.gardeniot.ui.home.HomeViewModel;
import com.example.gardeniot.ui.home.SendRequest;
import com.google.android.material.navigation.NavigationView;

import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.Socket;

import java.io.IOException;

/*
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;*/


public class DrawerMain extends AppCompatActivity {

    private HomeViewModel homeViewModel;


    private AppBarConfiguration mAppBarConfiguration;
    private FragmentManager fragmentManager;
    private int fragCount = 1;
    private String url;
    private RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //FloatingActionButton fab = findViewById(R.id.fab);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_stats)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);

        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Ajout d'un nouvel espace - en construction", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Log.d("Apres bouton clic", "on a cliqué");
                fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment newFrag = new FragmentGarden();
                Bundle argNav = new Bundle();
                argNav.putInt("cpt", ++fragCount);
                newFrag.setArguments(argNav);
                String cpt = "" + fragCount;
                fragmentTransaction.add(newFrag, cpt);
                //int idNewFrag = newFrag.getId();

                fragmentTransaction.commit();


            }
        });*/

        // Instantiate the RequestQueue.
        queue = Volley.newRequestQueue(this);
        url ="http://localhost:8080"; // à adapter. Plusieurs url possibles ?





        NavigationUI.setupWithNavController(navigationView, navController);

        /*Thread closeActivity = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    Log.d("Thread", "après 1 seconde");
                } catch (Exception e) {
                    e.getLocalizedMessage();
                }
            }
        });*/

        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        //WEBSOCKET
        Uri.Builder url = Uri.parse("ws://192.168.1.70:4000/socket/websocket").buildUpon();

        try {
            Socket socket = new Socket(url.build().toString());
            socket.connect();
            //TODO REGARDER COMMENT CHOPER L'USER (ou le SAVE)
            Channel channel = socket.chan("updates:" + "token", null);

            channel.join()
                    .receive("ignore", envelope -> afficherToast("FAILED TO JOIN"))
                    .receive("ok", envelope -> {

                        String humidite = envelope.getPayload().get("response").get("humidite").asText();
                        String temperature = envelope.getPayload().get("response").get("temperature").asText();

                        runOnUiThread(() -> {
                            homeViewModel.setmTextHumid(humidite);
                            homeViewModel.setmTextTemp(temperature);
                        });
                    });

            channel.on("humidite", envelope -> {
                String humidite = envelope.getPayload().get("humidite").asText();
                runOnUiThread(() -> {
                    homeViewModel.setmTextHumid(humidite);
                });
            });

            channel.on("temperature", envelope -> {
                String temperature = envelope.getPayload().get("temperature").asText();
                runOnUiThread(() ->
                        homeViewModel.setmTextTemp(temperature));
            });

            channel.on("luminosite", envelope -> {
                String luminosite = envelope.getPayload().get("response").get("luminosite").asText();
            });

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer_main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        new SendRequest(queue,url);

        return super.onOptionsItemSelected(item);
    }

    private void afficherToast(String message) {
        Toast t = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        t.show();
        Log.i("DrawerMain", "toasted message : " + message);
    }
}