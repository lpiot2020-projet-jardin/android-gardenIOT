package com.example.gardeniot.custoFrags;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class FragmentGardenViewModel extends ViewModel {

    private MutableLiveData<String> mTextHumid;
    private MutableLiveData<String> mTextTemp;
    private MutableLiveData<String> mTextLumin;

    public FragmentGardenViewModel() {
        // We need to see what kind of data is coming from the server
        mTextHumid = new MutableLiveData<>();
        mTextTemp = new MutableLiveData<>();
        mTextLumin = new MutableLiveData<>();
        //Humidity and Temperature value
        mTextHumid.setValue("70%");
        mTextTemp.setValue("30°C");
        //sun, cloudy or night (= in the shades). More values / pics ?
        mTextLumin.setValue("sun");

    }

    public LiveData<String> getTextHumid() {
        return mTextHumid;
    }
    public LiveData<String> getTextTemp() {
        return mTextTemp;
    }
    public LiveData<String> getTextLumin() {return mTextLumin; }
}