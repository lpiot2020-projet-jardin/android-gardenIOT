package com.example.gardeniot.custoFrags;



import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.Nullable;
import androidx.core.app.NavUtils;

import com.example.gardeniot.DrawerMain;
import com.example.gardeniot.R;
import com.google.android.material.navigation.NavigationView;

public class FragmentGarden extends Fragment {
    private MenuItem newFragItem;
    private Menu m;
    private NavigationView navigationView;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        int cpt = 0;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            cpt = bundle.getInt("cpt");
        }
        navigationView = getActivity().findViewById(R.id.nav_view);
        String s = "Espace " +cpt;
        m = navigationView.getMenu();
        int sizeM = m.size();
        newFragItem = m.add(0, sizeM, sizeM, s);

        newFragItem.setIcon(R.drawable.plant);
        newFragItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                boolean b = item.getItemId() == newFragItem.getItemId();
                Log.d("Boolean", " = "+b);
                Log.d("item1:", " = "+item.getItemId());
                Log.d("item2:", " = "+newFragItem.getItemId());

                return b;
            }
        });
        Log.d("Frag Garden", s);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.activity_main_drawer, m);
        super.onCreateOptionsMenu(m, inflater);

    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        /** Créer un fragment layout type pour les nouveaux espaces ? En incrémentant les ID ? **/
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
