package com.example.gardeniot.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.gardeniot.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public HomeFragment(){

    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        homeViewModel =
                new ViewModelProvider(getActivity()).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        TextView textViewHumid = root.findViewById(R.id.textView2);

        homeViewModel.getTextHumid().observe(getViewLifecycleOwner(), string -> {
            textViewHumid.setText(string + "%");
        });

        TextView textViewTemp = root.findViewById(R.id.textView4);
        homeViewModel.getTextTemp().observe(getViewLifecycleOwner(), string -> {
            textViewTemp.setText(string + "°C");
        });

        ImageView ImageLumin = root.findViewById(R.id.ImageViewLumin);
        homeViewModel.getTextLumin().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s.equalsIgnoreCase("Sun"))
                    ImageLumin.setImageResource(R.drawable.sun);
                else if (s.equalsIgnoreCase("Cloudy"))
                    ImageLumin.setImageResource(R.drawable.cloudy);
                else
                    ImageLumin.setImageResource(R.drawable.night);

            }
        });
        return root;
    }


}