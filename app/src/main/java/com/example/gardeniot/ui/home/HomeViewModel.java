package com.example.gardeniot.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mTextHumid;
    private MutableLiveData<String> mTextTemp;
    private MutableLiveData<String> mTextLumin;


    public void setmTextHumid(String string) {
        this.mTextHumid.setValue(string);
    }

    public void setmTextTemp(String string) {
        this.mTextTemp.setValue(string);
    }

   /* public void setmTextTemp(MutableLiveData<String> mTextTemp) {
        this.mTextTemp = mTextTemp;
    }

    public void setmTextLumin(MutableLiveData<String> mTextLumin) {
        this.mTextLumin = mTextLumin;
    }*/

    public HomeViewModel() {
        // We need to see what kind of data is coming from the server
        mTextHumid = new MutableLiveData<>();
        mTextTemp = new MutableLiveData<>();
        mTextLumin = new MutableLiveData<>();
        //Humidity and Temperature value
        mTextHumid.setValue("70");
        mTextTemp.setValue("30");
        //sun, cloudy or night (= in the shades). More values / pics ?
        mTextLumin.setValue("sun");

    }

    public LiveData<String> getTextHumid() {
        return mTextHumid;
    }
    public LiveData<String> getTextTemp() {
        return mTextTemp;
    }
    public LiveData<String> getTextLumin() {return mTextLumin; }


}